# Catálogo de produtos

Catálogo de produtos com Java e Spring Boot.

## product-ms

Neste microserviço é possível criar, alterar, visualizar e excluir um determinado produto, além de visualizar a lista de produtos atuais disponíveis. Também pode ser possível realizar a busca de produtos filtrando por *name, description e price*.
