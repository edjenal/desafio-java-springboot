package com.compassouol.edjenal;

import java.util.stream.Collectors;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
 

@SuppressWarnings({"unchecked","rawtypes"})
@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

	@Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {       
        String details = ex.getBindingResult().getAllErrors().stream()
        	      .map(n -> n.getDefaultMessage())
        	      .collect(Collectors.joining(", ", "", ""));
        
        ErrorResponse error = new ErrorResponse(HttpStatus.BAD_REQUEST.value(), details);
        return new ResponseEntity(error, HttpStatus.BAD_REQUEST);
    }
}
