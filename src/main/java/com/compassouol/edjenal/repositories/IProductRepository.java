package com.compassouol.edjenal.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.compassouol.edjenal.entities.ProductEntity;

@Repository
public interface IProductRepository extends JpaRepository<ProductEntity, Integer> {

}
