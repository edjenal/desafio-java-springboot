package com.compassouol.edjenal.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.compassouol.edjenal.dtos.ProductDTO;
import com.compassouol.edjenal.services.ProductService;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@Validated
public class ProductController {
	
	private final ProductService productService;
	
	@GetMapping("/products")
	public List<ProductDTO> getAll() {
		return this.productService.findAll();
	}
	
	@GetMapping("/products/search")
	public List<ProductDTO> getProductSearch(@RequestParam(value = "q") String desc, 
			@RequestParam(value = "min_price") Double minPrice,
			@RequestParam(value = "max_price") Double maxPrice) {
		return this.productService.findByFilters(desc, minPrice, maxPrice);
	}
	
	@GetMapping("/products/{id}")
	public ResponseEntity<ProductDTO> getProductId(@PathVariable(required = true) String id) {
		ProductDTO dto = this.productService.findById(id);
		if (dto != null) {
			return new ResponseEntity<ProductDTO>(dto, HttpStatus.OK);
		} else {
			return new ResponseEntity<ProductDTO>(HttpStatus.NOT_FOUND);
		}
	}
	
	@PostMapping("/products")
	public ResponseEntity<ProductDTO> postProduct(@Valid @RequestBody ProductDTO newDto) {
		ProductDTO dto = this.productService.create(newDto);
		return new ResponseEntity<ProductDTO>(dto, HttpStatus.CREATED);
	}
	
	@PutMapping("/products/{id}")
	public ResponseEntity<ProductDTO> putProduct(@Valid @RequestBody ProductDTO product, @PathVariable(required = true) String id) {
		ProductDTO dto = this.productService.update(product, id);
		if (dto != null) {
			return new ResponseEntity<ProductDTO>(dto, HttpStatus.OK);
		} else {
			return new ResponseEntity<ProductDTO>(HttpStatus.NOT_FOUND);
		}
	}
	
	@DeleteMapping("/products/{id}")
	public ResponseEntity<ProductDTO> deleteProduct(@PathVariable(required = true) String id) {
		boolean deleted = this.productService.deleteById(id);
		if (deleted) {
			return new ResponseEntity<ProductDTO>(HttpStatus.OK);
		} else {
			return new ResponseEntity<ProductDTO>(HttpStatus.NOT_FOUND);
		}
	}
	
}
