package com.compassouol.edjenal.services;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.compassouol.edjenal.dtos.ProductDTO;
import com.compassouol.edjenal.entities.ProductEntity;
import com.compassouol.edjenal.repositories.IProductRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class ProductService {

	private final IProductRepository productRepository;
	
	public List<ProductDTO> findAll() {
		List<ProductEntity> entities = this.productRepository.findAll();

		List<ProductDTO> dtos = entities.stream().map(entity -> this.convertEntityToDto(entity))
				.collect(Collectors.toList());

		return dtos;
	}
	
	public List<ProductDTO> findByFilters(String desc, Double minPrice, Double maxPrice) {
		List<ProductEntity> entities = this.productRepository.findAll();
		
		if (desc != null && !desc.isEmpty()) {
			entities = entities.stream()
					.filter(entity -> entity.getName().equals(desc) || entity.getDescription().equals(desc))
					.collect(Collectors.toList());
		}
		
		if (minPrice != null) {
			entities = entities.stream()
					.filter(entity -> entity.getPrice() >= minPrice)
					.collect(Collectors.toList());
		}
		
		if (maxPrice != null) {
			entities = entities.stream()
					.filter(entity -> entity.getPrice() <= maxPrice)
					.collect(Collectors.toList());
		}
		

		List<ProductDTO> dtos = entities.stream().map(entity -> this.convertEntityToDto(entity))
				.collect(Collectors.toList());

		return dtos;
	}

	public ProductDTO findById(String id) {
		try {
			Optional<ProductEntity> entity = this.productRepository.findById(Integer.valueOf(id));
			return this.convertEntityToDto(entity.get());
		} catch (NoSuchElementException e) {
			return null;
		}
	}

	public ProductDTO create(ProductDTO dto) {
		ProductEntity entity = this.convertDtoToEntity(dto);
		entity = this.productRepository.save(entity);
		return this.convertEntityToDto(entity);
	}
	
	public ProductDTO update(ProductDTO dto, String id) {
		ProductEntity entity = this.productRepository.findById(Integer.valueOf(id)).map(object -> {
			object.setName(dto.getName());
			object.setDescription(dto.getDescription());
			object.setPrice(dto.getPrice());
			return this.productRepository.save(object);
		}).orElse(null);
		
		if (entity != null) {
			return this.convertEntityToDto(entity);
		} else {
			return null;
		}
		
	}

	public boolean deleteById(String id) {
		try {
			this.productRepository.deleteById(Integer.valueOf(id));
			return true;
		} catch (EmptyResultDataAccessException e) {
			return false;
		}
	}
	
	private ProductEntity convertDtoToEntity(ProductDTO dto) {
		return ProductEntity.builder()
				.id(dto.getId()!=null ? Integer.valueOf(dto.getId()): null )
				.name(dto.getName())
				.description(dto.getDescription())
				.price(dto.getPrice())
				.build();
	}

	private ProductDTO convertEntityToDto(ProductEntity entity) {
		return ProductDTO.builder().
				id(String.valueOf(entity.getId()))
				.name(entity.getName())
				.description(entity.getDescription())
				.price(entity.getPrice())
				.build();
	}
}
