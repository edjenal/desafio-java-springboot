package com.compassouol.edjenal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EdjenalApplication {

	public static void main(String[] args) {
		SpringApplication.run(EdjenalApplication.class, args);
	}

}
