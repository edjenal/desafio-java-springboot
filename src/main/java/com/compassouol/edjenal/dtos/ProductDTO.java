package com.compassouol.edjenal.dtos;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class ProductDTO {
	
	private String id;
	
	@NotEmpty(message = "Name is mandatory")
	private String name;
	
	@NotEmpty(message = "Description is mandatory")
	private String description;
	
	@NotNull(message = "Price is mandatory")
	@Min(1)
	private Double price;
	
}
